# compilador LaTeX
LC:=pdflatex
LCFLAGS:=--halt-on-error

# visualizados de arquivos pdf
VIEWER:=evince

.PHONY: view


%.pdf: %.tex
	$(LC) $(LCFLAGS) $< && $(LC) $(LFLAGS) $<

a5.pdf: a5.tex

view: a5.pdf
	$(VIEWER) $<
